# appautoscale

## Overview
appautoscale is a system to extend and scale up applications.

### ScaleServer
ScaleServer is stateless,the caller needs to send ScaleMonitor objects to ScaleServer when there are requests for capacity expansion and scaling. ScaleServer can also provide services independently.

### Monitor
Monitor is designed to constantly determine whether to expand or shrink according to Metrics according to the user-defined ScaleMonitor. The current design is to run only one Monitor. In order to ensure that the user's registered ScaleMonitor is not lost after Monitor restart, It will save the user-registered ScaleMonitor to the ConfigMap under kube-system namespace. Each ScaleMonitor corresponds to a ConfigMap.

## Getting Started
* create deployment and service

```
kubectl create -f deploy.yaml
kubectl create -f svc.yaml
```
* use to scale

Register a ScaleMonitor
```
curl -k -u name:pwd https://ip:port/regist/patricktest -X POST -d @./samplejson/deploymentset.json
```
delete a ScaleMonitor
```
curl -k -u name:pwd https://ip:port/delete/patricktest/testdeploymentset
```
Query the registered ScaleMonitor:
```
curl -k -u name:pwd https://ip:port/list/patricktest
```
Query svc sorting situation:
```
curl -k -u name:pwd https://ip:port/svcsort/patricktest/testdeploymentset
```
update a ScaleMonitor
```
curl -k -u name:pwd https://ip:port/update/patricktest -X POST -d @./samplejson/deploymentset.json
```
Log out of a ScaleMonitor
```
curl -k -u name:pwd https://ip:port/unregist/patricktest/testdeploymentset
```
