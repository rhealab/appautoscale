package main

import (
	"flag"
	"fmt"
	"net"
	"time"
)

var (
	connnum         = flag.Int("connectnum", 1, "connect num to server")
	serverip        = flag.String("serveraddr", "127.0.0.1:12345", "server ip")
	durtaion        = flag.Duration("waitduration", 100*time.Second, "connect wait duration")
	connectDuration = flag.Duration("connduration", 0*time.Second, "duration between two connect")
)

func main() {
	flag.Parse()
	conns := make([]net.Conn, 0, *connnum)
	defer func() {
		for _, conn := range conns {
			conn.Close()
		}
	}()
	for i := 0; i < *connnum; i++ {
		conn, err := net.Dial("tcp", *serverip)
		if err != nil {
			fmt.Printf("conn error:%v", err)
			return
		}
		time.Sleep(*connectDuration)
		conns = append(conns, conn)
	}
	time.Sleep(*durtaion)
}
