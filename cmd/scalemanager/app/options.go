package options

import (
	"appautoscale/pkg/monitor/metricplugin"
	"appautoscale/pkg/monitor/metricplugin/opentsdb"
	"appautoscale/pkg/monitor/metricplugin/podhttp"
	"appautoscale/pkg/scalemanager/scaleplugin"
	"appautoscale/pkg/scalemanager/scaleplugin/deploymentreplica"
	"appautoscale/pkg/scalemanager/scaleplugin/deploymentset"
	"appautoscale/pkg/scalemanager/scaleplugin/replicasetreplica"
	"appautoscale/pkg/scalemanager/scaleplugin/replicasetset"
	"appautoscale/pkg/scalemanager/scaleplugin/replicationcontrollerreplica"
	"appautoscale/pkg/scalemanager/scaleplugin/replicationcontrollerset"

	"github.com/spf13/pflag"
)

type ScaleManagerConfig struct {
	HttpsAddr     string
	TokenFilePath string
	CertFilePath  string
	KeyFilePath   string
	Debug         bool
	Kubeconfig    string
	ContentType   string
	ScalePlugins  []scaleplugin.ScalePlugin
	MetricPlugins []metricplugin.MetricPlugin
}

func (smc *ScaleManagerConfig) AddFlags(fs *pflag.FlagSet) {
	fs.StringVar(&smc.HttpsAddr, "httpsaddr", smc.HttpsAddr, "Wait server to connect.")
	fs.StringVar(&smc.TokenFilePath, "tokenfilepath", smc.TokenFilePath, "The token for client server.")
	fs.StringVar(&smc.CertFilePath, "certfilepath", smc.CertFilePath, "The https cert for client server.")
	fs.StringVar(&smc.KeyFilePath, "keyfilepath", smc.KeyFilePath, "The https key for client server.")
	fs.StringVar(&smc.Kubeconfig, "kubeconfig", smc.Kubeconfig, "Path to kubeconfig file with authorization and master location information.")
	fs.StringVar(&smc.ContentType, "kube-api-content-type", smc.ContentType, "Content type of requests sent to apiserver.")
	fs.BoolVar(&smc.Debug, "debug", smc.Debug, "Display debug log.")
}

func ProbeScaleVolumePlugins() []scaleplugin.ScalePlugin {
	ret := []scaleplugin.ScalePlugin{}
	ret = append(ret, deploymentreplica.ProbeVolumePlugins()...)
	ret = append(ret, deploymentset.ProbeVolumePlugins()...)
	ret = append(ret, replicasetreplica.ProbeVolumePlugins()...)
	ret = append(ret, replicasetset.ProbeVolumePlugins()...)
	ret = append(ret, replicationcontrollerreplica.ProbeVolumePlugins()...)
	ret = append(ret, replicationcontrollerset.ProbeVolumePlugins()...)
	return ret
}
func ProbeMonitorVolumePlugins() []metricplugin.MetricPlugin {
	ret := []metricplugin.MetricPlugin{}
	ret = append(ret, opentsdb.ProbeVolumePlugins()...)
	ret = append(ret, podhttp.ProbeVolumePlugins()...)
	return ret
}
func NewScaleManagerConfig() *ScaleManagerConfig {
	return &ScaleManagerConfig{
		HttpsAddr:     ":3078",
		ContentType:   "application/vnd.kubernetes.protobuf",
		Debug:         false,
		ScalePlugins:  ProbeScaleVolumePlugins(),
		MetricPlugins: ProbeMonitorVolumePlugins(),
	}
}

func InitFlags() {
	pflag.Parse()
}
