package main

import (
	"appautoscale/common"
	"encoding/json"
	"fmt"
	"time"

	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/kubernetes/pkg/api/v1"
	extensions "k8s.io/kubernetes/pkg/apis/extensions/v1beta1"
	//"k8s.io/kubernetes/pkg/apis/extensions"
	//"k8s.io/kubernetes/pkg/apis/extensions/validation"
)

func printDeploymentReplica() {
	smDeploymentReplica := common.ScaleMoniter{}
	smDeploymentReplica.Name = "test"
	smDeploymentReplica.MaxReplics = 3
	smDeploymentReplica.MinReplics = 1
	smDeploymentReplica.MoniterMetrics = []common.MoniterMetric{
		common.MoniterMetric{
			Name:           "CPU",
			UpScaleValue:   80.0,
			DownScaleValue: 50.0,
			MetricsTime:    10 * time.Minute,
		},
		common.MoniterMetric{
			Name:           "Memory",
			UpScaleValue:   90.0,
			DownScaleValue: 60.0,
			MetricsTime:    5 * time.Minute,
		},
	}
	smDeploymentReplica.ScaleUpTrigger = "CPU || Memory"
	smDeploymentReplica.ScaleDownTrigger = "CPU && Memory"
	smDeploymentReplica.ScaleType = common.ScaleType("DeploymentReplica")
	key := "patricktest/test"
	smDeploymentReplica.ScaleTemplate = common.ScaleTemplate{
		DeploymentReplica: &key,
	}
	buf, _ := json.MarshalIndent(&smDeploymentReplica, " ", "  ")
	fmt.Printf("%s\n", string(buf))
}

func printDeploymentSet() {
	smDeploymentSet := common.ScaleMoniter{}
	smDeploymentSet.Name = "testdeploymentset"
	smDeploymentSet.MaxReplics = 3
	smDeploymentSet.MinReplics = 1
	smDeploymentSet.MoniterMetrics = []common.MoniterMetric{
		common.MoniterMetric{
			Name:           "connectnum",
			UpScaleValue:   20.0,
			DownScaleValue: 10.0,
			MetricsTime:    1 * time.Minute,
			MetricGeter: common.MetricGeter{
				OpenTsDBMetricGeter: &common.OpenTsDBMetricGeter{
					Url:    "http://127.0.0.1:4242/api/query",
					Metric: "connectnum",
					Tags:   map[string]string{"host": "${podname}"},
				},
			},
		},
	}
	smDeploymentSet.ScaleUpTrigger = "CPU || Memory"
	smDeploymentSet.ScaleDownTrigger = "CPU && Memory"
	smDeploymentSet.ScaleType = common.ScaleType("DeploymentSet")
	var replica int32 = 1
	smDeploymentSet.ScaleTemplate = common.ScaleTemplate{
		DeploymentSet: &extensions.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "testname",
				Namespace: "patricktest",
			},
			Spec: extensions.DeploymentSpec{
				Replicas: &replica,
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{"app": "testapp"},
					},
					Spec: v1.PodSpec{
						Containers: []v1.Container{
							v1.Container{
								Name:  "nginx",
								Image: "index.alauda.cn/library/nginx:stable",
								Resources: v1.ResourceRequirements{
									Requests: v1.ResourceList{
										v1.ResourceMemory: resource.MustParse("100Mi"),
										v1.ResourceCPU:    resource.MustParse("300m"),
									},
									Limits: v1.ResourceList{
										v1.ResourceMemory: resource.MustParse("100Mi"),
										v1.ResourceCPU:    resource.MustParse("300m"),
									},
								},
							},
						},
					},
				},
			},
		},
	}
	smDeploymentSet.SvcPool = &common.SvcPool{
		IndexAnnotationKey: "io.enndata.appautoscale/my_svc_index",
		Selector:           nil, //&metav1.LabelSelector{},
		SvcTemplate: &v1.Service{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "testname",
				Namespace: "patricktest",
			},
			Spec: v1.ServiceSpec{
				Ports: []v1.ServicePort{v1.ServicePort{
					Name:       "tcp",
					Protocol:   v1.ProtocolTCP,
					Port:       8080,
					TargetPort: intstr.IntOrString{IntVal: 80},
				}},
				Type:     v1.ServiceTypeClusterIP,
				Selector: map[string]string{"app": "testapp"},
			},
		},
	}

	buf, _ := json.MarshalIndent(&smDeploymentSet, " ", "  ")
	fmt.Printf("%s\n", string(buf))
}

func main() {
	//printDeploymentSet()
	fmt.Printf("%d\n", time.Now().Unix())
}
