package triggerparse

import (
	"fmt"
	"strings"
)

type Up interface {
	Name() string
	IsUp() (bool, error)
}
type Down interface {
	Name() string
	IsDown() (bool, error)
}

type Judge interface {
	Judge() bool
}

type UpJudge struct {
	up Up
}

func (uj *UpJudge) Judge() bool {
	ok, err := uj.up.IsUp()
	if ok == true && err == nil {
		return true
	}
	return false
}

type DownJudge struct {
	down Down
}

func (dj *DownJudge) Judge() bool {
	ok, err := dj.down.IsDown()
	if ok == true && err == nil {
		return true
	}
	return false
}

type fakeJudge struct {
}

func (fj *fakeJudge) Judge() bool {
	return true
}

type Op string

const (
	OpOr  Op = "|"
	OpAnd Op = "&"
)

type ByteType int

const (
	ByteTypeName ByteType = iota
	ByteTypeOp
	ByteTypeLeftB
	ByteTypeRightB
	ByteTypeSpace
	ByteTypeUnKnow
)

func getByteType(b byte) ByteType {
	switch {
	case b == '(':
		return ByteTypeLeftB
	case b == ')':
		return ByteTypeRightB
	case b == ' ':
		return ByteTypeSpace
	case b >= 'a' && b <= 'z':
		return ByteTypeName
	case b >= 'A' && b <= 'Z':
		return ByteTypeName
	case b >= '0' && b <= '9':
		return ByteTypeName
	case b == '|' || b == '&':
		return ByteTypeOp
	default:
		return ByteTypeUnKnow
	}
}

type TriggerJudge struct {
	judgeMap   map[string]Judge
	valueMap   map[string]bool
	triggerStr string
}

func (tj *TriggerJudge) judgeNameTest(name string) (value, isExist bool) {
	if _, exist := tj.judgeMap[name]; exist == true {
		return true, true
	}
	return false, false
}

func (tj *TriggerJudge) judgeName(name string) (value, isNotFound bool) {
	if tj.valueMap != nil {
		if v, exist := tj.valueMap[name]; exist == true {
			return v, true
		}
	}
	if j, exist := tj.judgeMap[name]; exist == true {
		v := j.Judge()
		tj.valueMap[name] = v
		return v, true
	}
	return false, false
}
func (tj *TriggerJudge) judge(a, b bool, op Op) bool {

	switch op {
	case OpOr:
		//	fmt.Printf("patrick debug judge %t %s %t : %t\n", a, op, b, a || b)
		return a || b
	case OpAnd:
		//	fmt.Printf("patrick debug judge %t %s %t : %t\n", a, op, b, a && b)
		return a && b
	}
	//fmt.Printf("patrick debug judge %s return false\n", op)
	return false
}
func (tj *TriggerJudge) Judge() bool {
	tj.valueMap = make(map[string]bool)
	ok, _ := tj.tryJudge(tj.judgeName)
	return ok
}

func (tj *TriggerJudge) tryJudge(judgeNameFun func(name string) (value, isNotFound bool)) (bool, error) {
	if tj.triggerStr == "" {
		return false, nil
	}
	valueStack := NewStack(100)
	opStack := NewStack(100)

	doJudge := func() error {
		b, eb := valueStack.Pop()
		a, ea := valueStack.Pop()
		op, eop := opStack.Pop()

		if eb != nil || ea != nil {
			return fmt.Errorf("unexpect err get value")
		}
		if eop != nil {
			return fmt.Errorf("unexpect get op")
		}
		va, _ := a.(bool)
		vb, _ := b.(bool)
		vop, _ := op.(string)
		//fmt.Printf("patrick debug get op %s\n", op)
		ret := tj.judge(va, vb, Op(vop))
		valueStack.Push(ret)
		return nil
	}
	var nameStartIndex = -1
	for i, b := range []byte(tj.triggerStr) {
		t := getByteType(b)
		if t == ByteTypeUnKnow {
			return false, fmt.Errorf("unknow %c", b)
		}
		if t == ByteTypeName {
			if nameStartIndex < 0 {
				nameStartIndex = i
			}
			continue
		} else {
			if nameStartIndex >= 0 {
				name := string(tj.triggerStr[nameStartIndex:i])
				v, exist := judgeNameFun(name)
				if exist == false {
					return false, fmt.Errorf("%s is not found", name)
				}
				valueStack.Push(v)
				//fmt.Printf("patrick debug push2 %s:%t\n", name, v)

				if ok, err := IsTopString(opStack, string(OpAnd)); ok == true && err == nil { // case : valueA & valueB
					err := doJudge()
					if err != nil {
						return false, err
					}
				}
			}
			nameStartIndex = -1
		}
		switch t {
		case ByteTypeOp:
			opStack.Push(string(b))

		case ByteTypeLeftB:
			opStack.Push(string(b))
		case ByteTypeRightB:
			if ok, err := IsTopString(opStack, "("); ok == true && err == nil { // case  : (value)
				opStack.Pop()
			} else if ok == false && err == nil { // case : ( value1 op value2 )
				err := doJudge()
				if err != nil {
					return false, err
				}
				if ok, err := IsTopString(opStack, "("); ok == true && err == nil {
					opStack.Pop()
				} else {
					return false, fmt.Errorf("unexpert2 ')'")
				}
			} else {
				return false, fmt.Errorf("unexpect1 ')'")
			}
		}
	}
	//fmt.Printf("patrick debug nameStartIndex=%d\n", nameStartIndex)
	if nameStartIndex >= 0 {
		name := string(tj.triggerStr[nameStartIndex:len(tj.triggerStr)])
		//fmt.Printf("patrick debug get name %s\n", name)
		v, exist := judgeNameFun(name)
		if exist == false {
			return false, fmt.Errorf("%s is not found", name)
		}
		//fmt.Printf("patrick debug push value %t\n", v)
		valueStack.Push(v)
		//fmt.Printf("patrick debug push %s:%t\n", name, v)
	}

	for {
		if opStack.IsEmpty() == true {
			if valueStack.Len() == 1 {
				ret, _ := valueStack.Pop()
				ok, _ := ret.(bool)
				return ok, nil
			} else {
				return false, fmt.Errorf("unknow err left %d value no operation", valueStack.Len())
			}
		} else {
			err := doJudge()
			if err != nil {
				return false, err
			}
		}
	}
	return false, fmt.Errorf("unknow error")
}

func TriggerStrValid(str string, metrics []string) error {
	str = strings.Replace(str, "||", "|", -1)
	str = strings.Replace(str, "&&", "&", -1)
	if err := triggerStringCheck(str); err != nil {
		return fmt.Errorf("check triggerstr:'%s' err:%v", str, err)
	}
	judges := make(map[string]Judge)
	for _, m := range metrics {
		judges[m] = &fakeJudge{}
	}
	test := &TriggerJudge{
		judgeMap:   judges,
		triggerStr: str,
	}
	_, err := test.tryJudge(test.judgeNameTest)
	return err
}

func NewDownTriggerJudge(triggerStr string, triggers []Down) (Judge, error) {
	triggerStr = strings.Replace(triggerStr, "||", "|", -1)
	triggerStr = strings.Replace(triggerStr, "&&", "&", -1)
	if err := triggerStringCheck(triggerStr); err != nil {
		return nil, fmt.Errorf("check triggerstr:'%s' err:%v", triggerStr, err)
	}
	downJudges := make(map[string]Judge)
	for _, t := range triggers {
		downJudges[t.Name()] = &DownJudge{down: t}
	}
	ret := &TriggerJudge{
		judgeMap:   downJudges,
		triggerStr: triggerStr,
	}
	_, err := ret.tryJudge(ret.judgeNameTest)
	if err != nil {
		return ret, err
	}
	return ret, nil
}

func NewUpTriggerJudge(triggerStr string, triggers []Up) (Judge, error) {
	triggerStr = strings.Replace(triggerStr, "||", "|", -1)
	triggerStr = strings.Replace(triggerStr, "&&", "&", -1)
	if err := triggerStringCheck(triggerStr); err != nil {
		return nil, fmt.Errorf("check triggerstr:'%s' err:%v", triggerStr, err)
	}
	upJudges := make(map[string]Judge)
	for _, t := range triggers {
		upJudges[t.Name()] = &UpJudge{up: t}
	}
	ret := &TriggerJudge{
		judgeMap:   upJudges,
		triggerStr: triggerStr,
	}
	_, err := ret.tryJudge(ret.judgeNameTest)
	if err != nil {
		return ret, err
	}
	return ret, nil
}

func triggerStringCheck(str string) error {
	leftBracketNum := 0
	for _, b := range []byte(str) {
		if b == '(' {
			leftBracketNum++
		} else if b == ')' {
			if leftBracketNum > 0 {
				leftBracketNum--
			} else {
				return fmt.Errorf("unexpect ')'")
			}
		}
	}
	if leftBracketNum > 0 {
		return fmt.Errorf("no found ')'")
	}
	return nil
}

func IsTopString(s *Stack, str string) (bool, error) {
	opTop, errTop := s.Top()
	if errTop != nil {
		return false, errTop
	}
	top, _ := opTop.(string)
	if top == str {
		return true, nil
	} else {
		return false, nil
	}
}

func IsTopBool(s *Stack, b bool) (bool, error) {
	opTop, errTop := s.Top()
	if errTop != nil {
		return false, errTop
	}
	top, _ := opTop.(bool)
	if top == b {
		return true, nil
	} else {
		return false, nil
	}
}

type Stack struct {
	datas []interface{}
	//mu       sync.Mutex
	curIndex int
}

func (s *Stack) IsEmpty() bool {
	return s.curIndex == 0
}

func (s *Stack) Top() (interface{}, error) {
	if s.IsEmpty() {
		return "", fmt.Errorf("is empty")
	}
	return s.datas[s.curIndex-1], nil
}
func (s *Stack) Len() int {
	return s.curIndex
}
func (s *Stack) Pop() (interface{}, error) {
	if s.IsEmpty() {
		return "", fmt.Errorf("is empty")
	}
	s.curIndex--
	return s.datas[s.curIndex], nil
}

func (s *Stack) Push(d interface{}) error {
	if s.curIndex >= len(s.datas) {
		return fmt.Errorf("stack is full")
	}
	s.datas[s.curIndex] = d
	s.curIndex++
	return nil
}

func NewStack(size int) *Stack {
	return &Stack{
		datas:    make([]interface{}, size),
		curIndex: 0,
	}
}
