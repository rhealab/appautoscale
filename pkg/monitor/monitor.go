package monitor

import (
	monitorapp "appautoscale/cmd/monitor/app"
	"appautoscale/common"
	"appautoscale/log"
	"appautoscale/pkg/monitor/metricplugin"
	"fmt"
	"os"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	v1core "k8s.io/client-go/kubernetes/typed/core/v1"
	clientv1 "k8s.io/client-go/pkg/api/v1"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	kube_record "k8s.io/client-go/tools/record"
	kube_api "k8s.io/kubernetes/pkg/api"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
	"k8s.io/kubernetes/pkg/client/leaderelection"
	"k8s.io/kubernetes/pkg/client/leaderelection/resourcelock"
)

func createClient(mc *monitorapp.MonitorConfig) (*clientset.Clientset, error) {
	kubeconfig, err := clientcmd.BuildConfigFromFlags("", mc.Kubeconfig)
	if err != nil {
		return nil, fmt.Errorf("unable to build config from flags: %v", err)
	}

	kubeconfig.ContentType = mc.ContentType

	cli, err := clientset.NewForConfig(restclient.AddUserAgent(kubeconfig, "enndata-clustermanager"))
	if err != nil {
		return nil, fmt.Errorf("invalid API configuration: %v", err)
	}
	return cli, nil
}
func Run(mc *monitorapp.MonitorConfig) error {
	if err := run(mc); err != nil {
		return fmt.Errorf("failed to run MonitorServer : %v", err)
	}
	return nil
}

func checkConfig(mc *monitorapp.MonitorConfig) error {
	filePathCheck := func(name, path string) error {
		if path == "" {
			return fmt.Errorf("%s is empty", name)
		}
		if common.IsHostPathExist(path) == false {
			return fmt.Errorf("%s path %s is not exist", name, path)
		}
		return nil
	}
	if mc.HttpsAddr == "" {
		return fmt.Errorf("httpsaddr should not be empty")
	}
	if mc.ScaleServerAddr == "" {
		return fmt.Errorf("scaleserveraddr should not be empty")
	}
	if mc.ScaleServerUsername == "" {
		return fmt.Errorf("scaleserverusername should not be empty")
	}
	if mc.ScaleServerPassword == "" {
		return fmt.Errorf("scaleserverpassword should not be empty")
	}

	if err := filePathCheck("kubeconfig", mc.Kubeconfig); err != nil {
		return err
	}
	if err := filePathCheck("tokenfilepath", mc.TokenFilePath); err != nil {
		return err
	}
	if err := filePathCheck("certfilepath", mc.CertFilePath); err != nil {
		return err
	}
	if err := filePathCheck("keyfilepath", mc.KeyFilePath); err != nil {
		return err
	}
	return nil
}
func run(mc *monitorapp.MonitorConfig) error {
	log.MyLogI("enter run MonitorServer")
	if err := checkConfig(mc); err != nil {
		return err
	}
	client, errCreateClient := createClient(mc)
	if errCreateClient != nil {
		return fmt.Errorf("createClient err:%v", errCreateClient)
	}

	ip, errIp := common.GetIPByInterfaceName(mc.AddrInterFaceName)
	if errIp != nil {
		return fmt.Errorf("GetIPByInterfaceName of %s err:%v", mc.AddrInterFaceName, errIp)
	}
	log.MyLogI("get %s ip :%s", mc.AddrInterFaceName, ip)
	record := createEventRecorder(client)

	run := func(stop <-chan struct{}) {
		log.MyLogI("start run ScaleManagerServer ")
		hs := NewHttpScale(mc.ScaleServerAddr, mc.ScaleServerUsername, mc.ScaleServerPassword)
		mwm := NewMonitorWorkManager(metricplugin.NewMetricManager(mc.MetricPlugins), client, hs)

		mm := NewMonitorManager(client, mwm, hs)
		if errLoad := mm.Reload(); errLoad != nil {
			log.MyLogE("load monitor from configmap err :%v", errLoad)
			return
		}
		installHttpServer(mc.HttpsAddr, mc.TokenFilePath, mc.CertFilePath, mc.KeyFilePath, mm)
		panic("unreachable")
	}

	hostname, errHostName := os.Hostname()
	if errHostName != nil {
		return fmt.Errorf("Failed to get hostname %v", errHostName)
	}

	rl := resourcelock.EndpointsLock{
		EndpointsMeta: metav1.ObjectMeta{
			Namespace: mc.SvcNamespace,
			Name:      mc.SvcName,
		},
		Client: client,
		LockConfig: resourcelock.ResourceLockConfig{
			Identity:      hostname,
			EventRecorder: record,
		},
		HostIP:   ip,
		HostName: hostname,
	}

	leaderelection.RunOrDie(leaderelection.LeaderElectionConfig{
		Lock:          &rl,
		LeaseDuration: 15 * time.Second,
		RenewDeadline: 10 * time.Second,
		RetryPeriod:   2 * time.Second,
		Callbacks: leaderelection.LeaderCallbacks{
			OnStartedLeading: run,
			OnStoppedLeading: func() {
				log.MyLogE("leaderelection lost")
			},
		},
	})
	return nil
}

func createEventRecorder(client *clientset.Clientset) kube_record.EventRecorder {
	eventBroadcaster := kube_record.NewBroadcaster()
	eventBroadcaster.StartLogging(log.MyLogI)
	eventBroadcaster.StartRecordingToSink(&v1core.EventSinkImpl{Interface: v1core.New(client.Core().RESTClient()).Events("")})
	return eventBroadcaster.NewRecorder(kube_api.Scheme, clientv1.EventSource{Component: "appautoscale"})
}
