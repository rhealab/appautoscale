package podhttp

import (
	"appautoscale/common"
	"encoding/json"
	//"appautoscale/log"
	"appautoscale/pkg/monitor/metricplugin"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	v1 "k8s.io/kubernetes/pkg/api/v1"
)

const (
	pluginName string = "podhttp"
)

func ProbeVolumePlugins() []metricplugin.MetricPlugin {
	return []metricplugin.MetricPlugin{
		&podHttp{},
	}
}

var _ metricplugin.MetricPlugin = &podHttp{}
var _ metricplugin.MetricGeter = &podHttpGeter{}

type retItem struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Err  string `json:"err"`
	Data string `json:"data"`
}
type podHttp struct {
}

func (ph *podHttp) PluginName() string {
	return pluginName
}

func (ph *podHttp) Init() error {
	return nil
}

func (ph *podHttp) NewMetricGeter(mg *common.MoniterMetric, pod *v1.Pod) (metricplugin.MetricGeter, error) {
	if mg.MetricGeter.PodHttpMetricGeter == nil {
		return nil, fmt.Errorf("MetricGeter.PodHttpMetricGeter == nil")
	}

	return &podHttpGeter{
		client:             createHttpClient(),
		lowerLimit:         mg.DownScaleValue,
		upperLimit:         mg.UpScaleValue,
		podHttpMetricGeter: mg.MetricGeter.PodHttpMetricGeter,
		pod:                pod,
	}, nil
}

type podHttpGeter struct {
	client             *http.Client
	lowerLimit         float64
	upperLimit         float64
	podHttpMetricGeter *common.PodHttpMetricGeter
	pod                *v1.Pod
}

func (phg *podHttpGeter) IsUp(duration time.Duration, minMetrics int) (bool, error) {
	avg, err := phg.GetLasterAvgValue(duration, minMetrics)
	if err != nil {
		return false, fmt.Errorf("GetLasterAvgValue err:%v", err)
	}
	return avg > phg.upperLimit, nil
}

func (phg *podHttpGeter) IsDown(duration time.Duration, minMetrics int) (bool, error) {
	avg, err := phg.GetLasterAvgValue(duration, minMetrics)
	if err != nil {
		return false, fmt.Errorf("GetLasterAvgValue err:%v", err)
	}
	return avg < phg.lowerLimit, nil
}

func (phg *podHttpGeter) GetUpperLimit() float64 {
	return phg.upperLimit
}

func (phg *podHttpGeter) GetLowerLimit() float64 {
	return phg.lowerLimit
}

func (phg *podHttpGeter) GetCurrentValue() (float64, error) {
	return phg.GetLasterAvgValue(1*time.Second, 1)
}

func (phg *podHttpGeter) GetLasterAvgValue(duration time.Duration, minMetrics int) (float64, error) {
	if phg.pod.Status.PodIP == "" {
		return 0.0, fmt.Errorf("pod %s:%s has no ip", phg.pod.Namespace, phg.pod.Name)
	}
	addr := strings.Replace(phg.podHttpMetricGeter.Path, "${podip}", phg.pod.Status.PodIP, -1)
	resp, errGet := phg.client.Get(addr)
	if errGet != nil {
		return 0.0, errGet
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return 0.0, fmt.Errorf("unknow status code :%d from %s", resp.StatusCode, addr)
	}
	buf, errRead := ioutil.ReadAll(resp.Body)
	if errRead != nil {
		return 0.0, fmt.Errorf("read from %s err:%v", errRead)
	}
	retItem := retItem{}
	errJson := json.Unmarshal(buf, &retItem)
	if errJson != nil {
		return 0.0, fmt.Errorf("Unmarshal %s err:%v", string(buf), errJson)
	}
	if retItem.Code != 200 {
		return 0.0, fmt.Errorf("return code =%d, err:%s", retItem.Code, retItem.Err)
	}
	value, errParse := strconv.ParseFloat(retItem.Data, 64)
	if errParse != nil {
		return 0.0, fmt.Errorf("parse [%s] to float64 error", string(buf))
	}
	return value, nil
}

func createHttpClient() *http.Client {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		Dial: func(netw, addr string) (net.Conn, error) {
			conn, err := net.DialTimeout(netw, addr, 30*time.Second) //设置建立连接超时
			if err != nil {
				return nil, err
			}
			conn.SetDeadline(time.Now().Add(30 * time.Second)) //设置发送接受数据超时
			return conn, nil
		},
	}
	return &http.Client{
		Transport: tr,
	}
}
