package metricplugin

import (
	"appautoscale/common"
	"appautoscale/common/triggerparse"
	"appautoscale/log"
	"fmt"
	"time"

	v1 "k8s.io/kubernetes/pkg/api/v1"
)

func toDownInterface(triggers []*MetricGeterList) []triggerparse.Down {
	ret := make([]triggerparse.Down, 0, len(triggers))
	for _, t := range triggers {
		ret = append(ret, t)
	}
	return ret
}
func toUpInterface(triggers []*MetricGeterList) []triggerparse.Up {
	ret := make([]triggerparse.Up, 0, len(triggers))
	for _, t := range triggers {
		ret = append(ret, t)
	}
	return ret
}

type MetricTrigger struct {
	Mgls        []*MetricGeterList
	Uptrigger   string
	Downtrigger string
}

func (mt *MetricTrigger) IsUp() (bool, error) {
	jd, errd := triggerparse.NewUpTriggerJudge(mt.Uptrigger, toUpInterface(mt.Mgls))
	if errd != nil {
		log.MyLogE("NewUpTriggerJudge %v", errd)
		return false, errd
	}
	return jd.Judge(), nil
}
func (mt *MetricTrigger) IsDown() (bool, error) {
	ju, erru := triggerparse.NewDownTriggerJudge(mt.Downtrigger, toDownInterface(mt.Mgls))
	if erru != nil {
		log.MyLogE("NewDownTriggerJudge %v", erru)
		return false, erru
	}
	return ju.Judge(), nil
}

type MetricGeterList struct {
	Key            string
	MetricName     string
	Getters        []MetricGeter
	Duration       time.Duration
	UpScaleValue   float64 `json:"upScaleValue"`
	DownScaleValue float64 `json:"downScaleValue"`
}

func (mgl MetricGeterList) Name() string {
	return mgl.MetricName
}

func (mgl MetricGeterList) IsUp() (bool, error) {
	var sum float64
	for _, getter := range mgl.Getters {
		if tmp, err := getter.GetLasterAvgValue(mgl.Duration, 1); err != nil {
			log.MyTagLogE(mgl.Key, "IsUp %s GetLasterAvgValue err:%v", mgl.MetricName, err)
			return false, fmt.Errorf("GetLasterAvgValue err:%v", err)
		} else {
			sum += tmp
		}
	}
	if sum > mgl.UpScaleValue*float64(len(mgl.Getters)) {
		log.MyTagLogD(mgl.Key, "%s cur value %.2f > %.2f*%d = %.2f is up", mgl.MetricName, sum, mgl.UpScaleValue, len(mgl.Getters), mgl.UpScaleValue*float64(len(mgl.Getters)))
		return true, nil
	} else {
		log.MyTagLogD(mgl.Key, "%s cur value %.2f <= %.2f*%d = %.2f no up", mgl.MetricName, sum, mgl.UpScaleValue, len(mgl.Getters), mgl.UpScaleValue*float64(len(mgl.Getters)))
	}
	return false, nil
}

func (mgl *MetricGeterList) IsDown() (bool, error) {
	var sum float64
	for _, getter := range mgl.Getters {
		if tmp, err := getter.GetLasterAvgValue(mgl.Duration, 1); err != nil {
			log.MyTagLogE(mgl.Key, "IsDown %s GetLasterAvgValue err:%v", mgl.MetricName, err)
			return false, fmt.Errorf("GetLasterAvgValue err:%v", err)
		} else {
			sum += tmp
		}
	}
	if sum <= mgl.DownScaleValue*float64(len(mgl.Getters)) {
		log.MyTagLogD(mgl.Key, "%s cur value %.2f <= %.2f*%d = %.2f is down", mgl.MetricName, sum, mgl.DownScaleValue, len(mgl.Getters), mgl.DownScaleValue*float64(len(mgl.Getters)))
		return true, nil
	} else {
		log.MyTagLogD(mgl.Key, "%s cur value %.2f > %.2f*%d = %.2f no down", mgl.MetricName, sum, mgl.DownScaleValue, len(mgl.Getters), mgl.DownScaleValue*float64(len(mgl.Getters)))
	}
	return false, nil
}
func (mgl *MetricGeterList) GetMetricScore() (score float64, err error) {
	var sum float64
	for _, getter := range mgl.Getters {
		if tmp, err := getter.GetLasterAvgValue(mgl.Duration, 1); err != nil {
			log.MyTagLogE(mgl.Key, "GetMetricScore %s GetLasterAvgValue err:%v", mgl.MetricName, err)
			return 0, fmt.Errorf("GetLasterAvgValue err:%v", err)
		} else {
			sum += (tmp - mgl.DownScaleValue) / (mgl.UpScaleValue - mgl.DownScaleValue)
			log.MyTagLogD(mgl.Key, "(%.2f - %.2f)/ (%.2f - %.2f) = %.2f", tmp, mgl.DownScaleValue, mgl.UpScaleValue, mgl.DownScaleValue, (tmp-mgl.DownScaleValue)/(mgl.UpScaleValue-mgl.DownScaleValue))
		}
	}
	return sum, nil
}

type MetricGeter interface {
	IsUp(duration time.Duration, minMetrics int) (bool, error)
	IsDown(duration time.Duration, minMetrics int) (bool, error)
	GetUpperLimit() float64
	GetLowerLimit() float64
	GetCurrentValue() (float64, error)
	GetLasterAvgValue(duration time.Duration, minMetrics int) (float64, error)
}

type MetricPlugin interface {
	PluginName() string
	Init() error
	NewMetricGeter(mg *common.MoniterMetric, pod *v1.Pod) (MetricGeter, error)
}

type MetricManager struct {
	plugins map[string]MetricPlugin
}

func (mm *MetricManager) InitPlugin(plugins []MetricPlugin) {
	for _, plugin := range plugins {
		name := plugin.PluginName()
		if name == "" {
			continue
		}
		if err := plugin.Init(); err != nil {
			log.MyLogE("init plugin %s err:%v", name, err)
			continue
		}
		mm.plugins[name] = plugin
	}
}

func (mm *MetricManager) FindPluginByName(name string) (MetricPlugin, error) {
	plugin, exist := mm.plugins[name]
	if exist == true {
		return plugin, nil
	}
	return nil, fmt.Errorf("plugin %s is not found", name)
}

func (mm *MetricManager) FindPlugin(mg *common.MetricGeter) (MetricPlugin, error) {
	if mg.OpenTsDBMetricGeter != nil {
		return mm.FindPluginByName("opentsdb")
	}
	if mg.PodHttpMetricGeter != nil {
		return mm.FindPluginByName("podhttp")
	}
	return nil, fmt.Errorf("plugin is not found")
}

func NewMetricManager(plugins []MetricPlugin) *MetricManager {
	mm := &MetricManager{
		plugins: make(map[string]MetricPlugin),
	}
	mm.InitPlugin(plugins)
	return mm
}
