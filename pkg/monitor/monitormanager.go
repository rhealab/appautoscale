package monitor

import (
	"appautoscale/common"
	"appautoscale/pkg/scalemanager/scaleplugin"
	//"appautoscale/log"
	"encoding/json"
	"fmt"
	"strings"
	"sync"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/kubernetes/pkg/api/v1"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

const (
	scaleMoniterConfigMapPrefix        = "scalemoniter"
	scaleMoniterConfigMapNamespace     = "kube-system"
	scaleMoniterConfigMap_itemname     = "scalemoniter.json"
	scaleMoniterConfigMap_describename = "readme"
	scaleMoniterConfigMap_describe     = "it's created by appautoscale monitor to save ScaleMoniter, please not delete it"
)

func getScaleMoniterConfigMapName(ns, name string) string {
	return fmt.Sprintf("%s-%s-%s", scaleMoniterConfigMapPrefix, ns, name)
}

func isScaleMoniterConfigMap(name string) bool {
	return strings.HasPrefix(name, scaleMoniterConfigMapPrefix)
}

//func getScaleMoniterConfigMapInfoByName(str string) (ns, name string) {
//	strs := strings.Split(str, "-")
//	if len(strs) == 3 {
//		return strs[1], strs[2]
//	}
//	return "", ""
//}

type MonitorManagerInferface interface {
	GetMonitor(namespace, name string) (*common.ScaleMoniter, error)
	ListMonitor(namespace string) ([]*common.ScaleMoniter, error)
	ListAllMonitors() ([]*common.ScaleMoniter, error)
	DeleteMonitor(namespace, name string, recycleResouce bool) error
	RegistMonitor(namespace string, sm *common.ScaleMoniter) error
	UpdateMonitor(namespace string, sm *common.ScaleMoniter) error
	ListRetainServices(namespace string, sm *common.ScaleMoniter) ([]*v1.Service, error)
}

type MonitorManager struct {
	client     *clientset.Clientset
	monitorMap map[string]*common.ScaleMoniter
	mu         sync.Mutex
	mwm        *MonitorWorkManager
	hs         *HttpScale
}

func NewMonitorManager(client *clientset.Clientset, mwm *MonitorWorkManager, hs *HttpScale) *MonitorManager {
	ret := &MonitorManager{
		client:     client,
		mwm:        mwm,
		hs:         hs,
		monitorMap: make(map[string]*common.ScaleMoniter),
	}
	return ret
}

func (mm *MonitorManager) deleteMonitorConfigMap(namespace, name string) error {
	cmName := getScaleMoniterConfigMapName(namespace, name)
	return mm.client.CoreV1().ConfigMaps(scaleMoniterConfigMapNamespace).Delete(cmName, nil)
}

func (mm *MonitorManager) updateMonitorConfigMap(namespace, name string, sm *common.ScaleMoniter) error {
	cmName := getScaleMoniterConfigMapName(namespace, name)
	buf, errJson := json.MarshalIndent(sm, " ", "  ")
	if errJson != nil {
		return errJson
	}
	cm := &v1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: scaleMoniterConfigMapNamespace,
			Name:      cmName,
		},
		Data: map[string]string{
			scaleMoniterConfigMap_itemname:     string(buf),
			scaleMoniterConfigMap_describename: scaleMoniterConfigMap_describe,
		},
	}

	_, errUpdate := mm.client.CoreV1().ConfigMaps(cm.Namespace).Update(cm)
	return errUpdate
}

func (mm *MonitorManager) createMonitorConfigMap(namespace, name string, sm *common.ScaleMoniter) error {
	cmName := getScaleMoniterConfigMapName(namespace, name)
	buf, errJson := json.MarshalIndent(sm, " ", "  ")
	if errJson != nil {
		return errJson
	}
	cm := &v1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: scaleMoniterConfigMapNamespace,
			Name:      cmName,
		},
		Data: map[string]string{
			scaleMoniterConfigMap_itemname:     string(buf),
			"namespace":                        namespace,
			scaleMoniterConfigMap_describename: scaleMoniterConfigMap_describe,
		},
	}

	_, errCreate := mm.client.CoreV1().ConfigMaps(cm.Namespace).Create(cm)
	return errCreate
}

func (mm *MonitorManager) loadFromConfigMap(cm *v1.ConfigMap) error {
	jsonStr := cm.Data[scaleMoniterConfigMap_itemname]
	ns := cm.Data["namespace"]
	sm := common.ScaleMoniter{}
	err := json.Unmarshal([]byte(jsonStr), &sm)
	if err != nil {
		return err
	}
	//ns, name := getScaleMoniterConfigMapInfoByName(cm.Name)
	name := sm.Name
	mm.mu.Lock()
	mm.mu.Unlock()
	key := common.GetKeyByNamespaceAndName(ns, name)
	mm.monitorMap[key] = &sm
	mm.mwm.AddMonitor(ns, &sm)
	return nil
}

func (mm *MonitorManager) Reload() error {
	list, err := mm.client.CoreV1().ConfigMaps(scaleMoniterConfigMapNamespace).List(metav1.ListOptions{})
	if err != nil {
		return err
	}
	for i := range list.Items {
		cm := &list.Items[i]
		if isScaleMoniterConfigMap(cm.Name) {
			mm.loadFromConfigMap(cm)
		}
	}
	return nil
}

func (mm *MonitorManager) GetMonitor(namespace, name string) (*common.ScaleMoniter, error) {
	mm.mu.Lock()
	defer mm.mu.Unlock()
	key := common.GetKeyByNamespaceAndName(namespace, name)
	if ret, exist := mm.monitorMap[key]; exist == true {
		return ret, nil
	} else {
		exist := make([]string, 0, len(mm.monitorMap))
		for key := range mm.monitorMap {
			exist = append(exist, key)
		}
		return nil, fmt.Errorf("%s monitor is not found exist:%v", key, exist)
	}
}

func (mm *MonitorManager) DeleteMonitor(namespace, name string, recycleResource bool) error {
	mm.mu.Lock()
	defer mm.mu.Unlock()
	key := common.GetKeyByNamespaceAndName(namespace, name)
	if sm, exist := mm.monitorMap[key]; exist == true {
		if recycleResource {
			errClean := mm.hs.Clean(namespace, sm)
			if errClean != nil {
				return fmt.Errorf("clean monitor %s err:%v", sm.Name, errClean)
			}
		}
		errDelete := mm.deleteMonitorConfigMap(namespace, name)
		if errDelete != nil {
			return errDelete
		}
		delete(mm.monitorMap, key)
		mm.mwm.RemoveMonitor(namespace, name)
		return nil
	} else {
		return fmt.Errorf("%s monitor is not found", key)
	}
}

func (mm *MonitorManager) ListMonitor(namespace string) ([]*common.ScaleMoniter, error) {
	mm.mu.Lock()
	defer mm.mu.Unlock()
	ret := make([]*common.ScaleMoniter, 0, len(mm.monitorMap))
	for key, m := range mm.monitorMap {
		ns, _ := common.GetNamespaceAndNameByKey(key)
		if namespace == ns {
			ret = append(ret, m)
		}
	}
	return ret, nil
}

func (mm *MonitorManager) ListRetainServices(namespace string, sm *common.ScaleMoniter) ([]*v1.Service, error) {
	if sm.SvcPool == nil {
		return []*v1.Service{}, fmt.Errorf("svcPool of %s is not define", sm.Name)
	}
	indexs, scores, err := mm.mwm.getServiceMetricSortIndex(sm)
	if err != nil {
		return []*v1.Service{}, err
	}
	_, replica, errGetCurReplica := mm.hs.GetCurReplica(namespace, sm)
	if errGetCurReplica != nil {
		return []*v1.Service{}, fmt.Errorf("GetCurReplica err:%v", errGetCurReplica)
	}
	status := mm.mwm.GetScaleMoniterScaleStatus(namespace, sm)
	if status == ScaleStatusDownErrNotMatch && replica > sm.MinReplics {
		replica = replica - 1
	}
	if replica < len(indexs) {
		indexs = indexs[:replica]
	}
	svcPool := scaleplugin.NewServicePool(namespace, sm.SvcPool, mm.client)
	svcs, errGet := svcPool.GetServicesByIndexs(indexs)
	if errGet != nil {
		return []*v1.Service{}, errGet
	}
	for i := range svcs {
		if svcs[i].Annotations == nil {
			svcs[i].Annotations = make(map[string]string)
		}
		svcs[i].Annotations[common.SvcScoreAnn] = fmt.Sprintf("%.2f", scores[i])
	}
	return svcs, nil
}

func (mm *MonitorManager) ListAllMonitors() ([]*common.ScaleMoniter, error) {
	mm.mu.Lock()
	defer mm.mu.Unlock()
	ret := make([]*common.ScaleMoniter, 0, len(mm.monitorMap))
	for _, m := range mm.monitorMap {
		ret = append(ret, m)
	}
	return ret, nil
}
func (mm *MonitorManager) UpdateMonitor(namespace string, sm *common.ScaleMoniter) error {
	mm.mu.Lock()
	defer mm.mu.Unlock()
	name := sm.Name
	key := common.GetKeyByNamespaceAndName(namespace, name)
	if _, exist := mm.monitorMap[key]; exist == false {
		return fmt.Errorf("%s monitor is not exist", key)
	}
	err := mm.updateMonitorConfigMap(namespace, name, sm)
	if err != nil {
		return fmt.Errorf("updateMonitorConfigMap %s err:%v", key, err)
	}
	mm.monitorMap[key] = sm
	mm.mwm.RemoveMonitor(namespace, name)
	mm.mwm.AddMonitor(namespace, sm)
	return nil
}

func (mm *MonitorManager) RegistMonitor(namespace string, sm *common.ScaleMoniter) error {
	mm.mu.Lock()
	defer mm.mu.Unlock()
	name := sm.Name
	key := common.GetKeyByNamespaceAndName(namespace, name)
	if _, exist := mm.monitorMap[key]; exist == true {
		return fmt.Errorf("%s monitor is exist", key)
	}
	err := mm.createMonitorConfigMap(namespace, name, sm)
	if err != nil {
		return fmt.Errorf("createMonitorConfigMap %s err:%v", key, err)
	}
	mm.monitorMap[key] = sm
	mm.mwm.AddMonitor(namespace, sm)
	return nil
}
