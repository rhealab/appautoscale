package monitor

import (
	"appautoscale/common"
	"appautoscale/log"
	"appautoscale/version"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"k8s.io/apiserver/pkg/authentication/authenticator"
	"k8s.io/apiserver/plugin/pkg/authenticator/password/passwordfile"
	"k8s.io/apiserver/plugin/pkg/authenticator/request/basicauth"
)

type httpServer struct {
	mm MonitorManagerInferface
}

func getScaleMoniterFromBody(read io.ReadCloser) (*common.ScaleMoniter, error) {
	result, _ := ioutil.ReadAll(read)
	read.Close()
	sm := &common.ScaleMoniter{}
	err := json.Unmarshal(result, sm)
	if err != nil {
		log.MyLogE("Unmarshal %s err:%v", string(result), err)
		return nil, err
	}
	return sm, common.ValidationScaleMoniter(sm)
}
func checkCanUpdate(newS, oldS *common.ScaleMoniter) error {
	if reflect.DeepEqual(newS.ScaleTemplate, oldS.ScaleTemplate) == true {
		return nil
	}
	return fmt.Errorf("ScaleTemplate cann't be update, please delete then create")
}
func (hs *httpServer) updateMonitor(res http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		log.MyLogD("update RequestURI:%s", req.RequestURI)
		strs := common.FilterEmptyString(strings.Split(req.RequestURI, "/"))
		if len(strs) != 2 {
			writeResponse(res, fmt.Errorf("unknow requsturi:%s", req.RequestURI), "", "update fail", nil)
			return
		}
		namespace := strs[1]

		sm, errParse := getScaleMoniterFromBody(req.Body)
		if errParse != nil {
			writeResponse(res, errParse, "", "update fail", nil)
			return
		}
		oldSm, errGet := hs.mm.GetMonitor(namespace, sm.Name)
		if errGet != nil {
			writeResponse(res, errGet, "", "update fail", nil)
			return
		}
		if errCheck := checkCanUpdate(sm, oldSm); errCheck != nil {
			writeResponse(res, errCheck, "", "update fail", nil)
			return
		}
		log.MyLogD("update namespace:%s, monitor:%v", namespace, sm)
		errUpdate := hs.mm.UpdateMonitor(namespace, sm)
		writeResponse(res, errUpdate, "update success", "update fail", nil)
	} else {
		writeResponse(res, fmt.Errorf("not support http method:%s", req.Method), "", "update fail", nil)
	}
}

func (hs *httpServer) unregist(res http.ResponseWriter, req *http.Request) {
	log.MyLogD("unregist RequestURI:%s", req.RequestURI)
	strs := common.FilterEmptyString(strings.Split(req.RequestURI, "/"))
	if len(strs) != 3 {
		writeResponse(res, fmt.Errorf("unknow requsturi:%s", req.RequestURI), "", "unregist fail", nil)
		return
	}
	namespace := strs[1]
	name := strs[2]
	log.MyLogD("unregist %s:%s", namespace, name)
	errDelete := hs.mm.DeleteMonitor(namespace, name, false)
	writeResponse(res, errDelete, "unregist success", "unregist fail", nil)
}

func (hs *httpServer) regist(res http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		log.MyLogD("regist RequestURI:%s", req.RequestURI)
		strs := common.FilterEmptyString(strings.Split(req.RequestURI, "/"))
		if len(strs) != 2 {
			writeResponse(res, fmt.Errorf("unknow requsturi:%s", req.RequestURI), "", "regist fail", nil)
			return
		}
		namespace := strs[1]

		sm, errParse := getScaleMoniterFromBody(req.Body)
		if errParse != nil {
			writeResponse(res, errParse, "", "regist fail", nil)
			return
		}

		log.MyLogD("regist namespace:%s, monitor:%v", namespace, sm)
		errRegist := hs.mm.RegistMonitor(namespace, sm)
		writeResponse(res, errRegist, "regist success", "regist fail", nil)
	} else {
		writeResponse(res, fmt.Errorf("not support http method:%s", req.Method), "", "regist fail", nil)
	}
}
func (hs *httpServer) getMonitor(res http.ResponseWriter, req *http.Request) {
	log.MyLogD("getMonitor RequestURI:%s", req.RequestURI)
	strs := common.FilterEmptyString(strings.Split(req.RequestURI, "/"))
	if len(strs) != 3 {
		writeResponse(res, fmt.Errorf("unknow requsturi:%s", req.RequestURI), "", "get fail", nil)
		return
	}
	namespace := strs[1]
	name := strs[2]
	log.MyLogD("getMonitor %s:%s", namespace, name)
	sm, errGet := hs.mm.GetMonitor(namespace, name)
	writeResponse(res, errGet, "get success", "get fail", sm)
}

func (hs *httpServer) listService(res http.ResponseWriter, req *http.Request) {
	log.MyLogD("listService RequestURI:%s", req.RequestURI)
	strs := common.FilterEmptyString(strings.Split(req.RequestURI, "/"))
	if len(strs) != 3 {
		writeResponse(res, fmt.Errorf("unknow requsturi:%s", req.RequestURI), "", "get fail", nil)
		return
	}
	namespace := strs[1]
	name := strs[2]
	sm, errGet := hs.mm.GetMonitor(namespace, name)
	if errGet != nil || sm == nil {
		writeResponse(res, errGet, "", "listService fail", nil)
		return
	}
	if sm.SvcPool == nil || (sm.SvcPool.Selector == nil && sm.SvcPool.SvcTemplate == nil) {
		writeResponse(res, fmt.Errorf("%s SvcPool is not define", sm.Name), "", "listService fail", nil)
		return
	}
	svcs, err := hs.mm.ListRetainServices(namespace, sm)
	if err != nil {
		writeResponse(res, fmt.Errorf("ListRetainServices of monitor, err:%v", sm.Name, err), "", "listService fail", nil)
		return
	}
	writeResponse(res, nil, "listService success", "listService fail", svcs)
}

func (hs *httpServer) listMonitor(res http.ResponseWriter, req *http.Request) {
	log.MyLogD("listMonitor RequestURI:%s", req.RequestURI)
	strs := common.FilterEmptyString(strings.Split(req.RequestURI, "/"))
	if len(strs) != 2 {
		writeResponse(res, fmt.Errorf("unknow requsturi:%s", req.RequestURI), "", "list fail", nil)
		return
	}
	namespace := strs[1]
	log.MyLogD("listMonitor %s", namespace)
	sms, errList := hs.mm.ListMonitor(namespace)
	writeResponse(res, errList, "list success", "list fail", sms)
}

func (hs *httpServer) deleteMonitor(res http.ResponseWriter, req *http.Request) {
	log.MyLogD("deleteMonitor RequestURI:%s", req.RequestURI)
	strs := common.FilterEmptyString(strings.Split(req.RequestURI, "/"))
	if len(strs) != 3 {
		writeResponse(res, fmt.Errorf("unknow requsturi:%s", req.RequestURI), "", "delete fail", nil)
		return
	}
	namespace := strs[1]
	name := strs[2]
	log.MyLogD("deleteMonitor %s:%s", namespace, name)
	errDelete := hs.mm.DeleteMonitor(namespace, name, true)
	writeResponse(res, errDelete, "delete success", "delete fail", nil)
}
func installHttpServer(serverAddr, tokenfile, certfile, keyfile string, mm MonitorManagerInferface) error {
	mux := http.NewServeMux()
	auth, err := newAuthenticatorFromBasicAuthFile(tokenfile)
	if err != nil {
		log.MyLogE("Unable to installHttpServer: %v", err)
		return err
	}

	handler := WithAuthentication(mux, auth)
	mux.HandleFunc("/version", Version)

	httpServer := &httpServer{mm: mm}

	mux.HandleFunc("/regist/", httpServer.regist)
	mux.HandleFunc("/unregist/", httpServer.unregist)
	mux.HandleFunc("/get/", httpServer.getMonitor)
	mux.HandleFunc("/list/", httpServer.listMonitor)
	mux.HandleFunc("/update/", httpServer.updateMonitor)
	mux.HandleFunc("/delete/", httpServer.deleteMonitor)
	mux.HandleFunc("/svcsort/", httpServer.listService)

	server := &http.Server{Addr: serverAddr, Handler: handler}
	return server.ListenAndServeTLS(certfile, keyfile)
}

func writeResponse(w io.Writer, err error, msgOk, msgFail string, object interface{}) {
	var retCode int
	var retErrStr string
	var objStr string
	var msg string
	if err != nil {
		retCode = 201
		retErrStr = err.Error()
		msg = msgFail
	} else {
		retCode = 200
		retErrStr = ""
		msg = msgOk
	}
	if object != nil && err == nil {
		buf, _ := json.MarshalIndent(object, " ", "  ")
		objStr = string(buf)

	}

	tmp := fmt.Sprintf("{\"code\":%d,\"msg\":\"%s\",\"err\":\"%s\", \"data\":\"%s\"}",
		retCode, msg, retErrStr, strings.Replace(objStr, "\"", "\\\"", -1))
	w.Write([]byte(tmp))
}

func Version(res http.ResponseWriter, req *http.Request) {
	res.Write([]byte(version.Get().String()))
}

// newAuthenticatorFromBasicAuthFile returns an authenticator.Request or an error
func newAuthenticatorFromBasicAuthFile(basicAuthFile string) (authenticator.Request, error) {
	basicAuthenticator, err := passwordfile.NewCSV(basicAuthFile)
	if err != nil {
		return nil, err
	}

	return basicauth.New(basicAuthenticator), nil
}

func WithAuthentication(handler http.Handler, auth authenticator.Request) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		_, ok, err := auth.AuthenticateRequest(req)
		if ok == false || err != nil {
			if err != nil {
				log.MyLogE("Unable to authenticate the request due to an error: %v", err)
			}
			unauthorizedBasicAuth(w, req)
			return
		}
		handler.ServeHTTP(w, req)
	})
}

// unauthorizedBasicAuth serves an unauthorized message to clients.
func unauthorizedBasicAuth(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("WWW-Authenticate", `Basic realm="kubernetes-master"`)
	http.Error(w, "Unauthorized", http.StatusUnauthorized)
}
