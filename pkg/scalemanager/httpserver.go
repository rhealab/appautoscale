package scalemanager

import (
	"appautoscale/common"
	"appautoscale/log"
	"appautoscale/pkg/scalemanager/scaleplugin"
	"appautoscale/version"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"k8s.io/apiserver/pkg/authentication/authenticator"
	"k8s.io/apiserver/plugin/pkg/authenticator/password/passwordfile"
	"k8s.io/apiserver/plugin/pkg/authenticator/request/basicauth"
)

type httpServer struct {
	scaleManager *scaleplugin.ScaleManager
}

func (hs *httpServer) doScaleMoniter(sm *common.ScaleMoniter, replica, timeOut int, isClean bool) error {
	scalePlugin, err := hs.scaleManager.FindPluginByName(string(sm.ScaleType))
	if err != nil {
		return fmt.Errorf("no plugin for scaleType %s, err:%v", sm.ScaleType, err)
	}
	scale, errNew := scalePlugin.NewScale(sm) //&sm.ScaleTemplate, sm.SvcPool, sm.MaxReplics, sm.MinReplics, sm.CanDeleteTrigger, sm.MoniterMetrics)
	if errNew != nil {
		return fmt.Errorf("newScale err:%v", errNew)
	}
	log.MyLogD("doScaleMoniter use plugin:%s", scalePlugin.PluginName())

	if timeOut > 0 {
		return scale.WaitScaleTo(replica, time.Duration(timeOut)*time.Millisecond)
	}
	if isClean == false {
		return scale.ScaleTo(replica)
	} else {
		return scale.Clean()
	}
}
func (hs *httpServer) getReplica(res http.ResponseWriter, req *http.Request) {
	sm, errParse := getScaleMoniterFromBody(req.Body)
	if errParse != nil {
		writeResponse(res, errParse, "", "getReplica fail", nil)
		return
	}

	scalePlugin, errFind := hs.scaleManager.FindPluginByName(string(sm.ScaleType))
	if errFind != nil {
		writeResponse(res, errFind, "", "getReplica fail", nil)
		return
	}
	scale, errNew := scalePlugin.NewScale(sm) //&sm.ScaleTemplate, sm.SvcPool, sm.MaxReplics, sm.MinReplics, sm.CanDeleteTrigger, sm.MoniterMetrics)
	if errNew != nil {
		writeResponse(res, errNew, "", "getReplica fail", nil)
		return
	}
	cur, wanted, err := scale.GetCurReplica()
	if err != nil {
		writeResponse(res, err, "", "getReplica fail", nil)
		return
	}
	writeResponse(res, nil, "isScale success", "", &struct {
		CurReplica    int `json:"curReplica"`
		DesireReplica int `json:"desireReplica"`
	}{CurReplica: cur, DesireReplica: wanted})
}

func (hs *httpServer) isScale(res http.ResponseWriter, req *http.Request) {
	sm, errParse := getScaleMoniterFromBody(req.Body)
	if errParse != nil {
		writeResponse(res, errParse, "", "isScale fail", nil)
		return
	}

	scalePlugin, errFind := hs.scaleManager.FindPluginByName(string(sm.ScaleType))
	if errFind != nil {
		writeResponse(res, errFind, "", "isScale fail", nil)
		return
	}
	scale, errNew := scalePlugin.NewScale(sm) //&sm.ScaleTemplate, sm.SvcPool, sm.MaxReplics, sm.MinReplics, sm.CanDeleteTrigger, sm.MoniterMetrics)
	if errNew != nil {
		writeResponse(res, errNew, "", "isScale fail", nil)
		return
	}
	ok, err := scale.IsScaling()
	if err != nil {
		writeResponse(res, err, "", "isScale fail", nil)
		return
	}
	writeResponse(res, nil, "isScale success", "", &struct {
		IsScaling bool `json:"isScaling"`
	}{ok})
}

func (hs *httpServer) scale(res http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		log.MyLogD("scale RequestURI:%s", req.RequestURI)
		strs := common.FilterEmptyString(strings.Split(req.RequestURI, "/"))
		if len(strs) != 3 {
			writeResponse(res, fmt.Errorf("unknow requsturi:%s", req.RequestURI), "", "scale fail", nil)
			return
		}
		timeOut, err1 := strconv.Atoi(strs[1])
		if err1 != nil {
			writeResponse(res, fmt.Errorf("read timeout err:%v", err1), "", "scale fail", nil)
			return
		}
		replica, err2 := strconv.Atoi(strs[2])
		if err2 != nil {
			writeResponse(res, fmt.Errorf("read replica err:%v", err2), "", "scale fail", nil)
			return
		}

		log.MyLogD("scale replica:%d, timeout:%d", replica, timeOut)

		sm, errParse := getScaleMoniterFromBody(req.Body)
		if errParse != nil {
			writeResponse(res, errParse, "", "scale fail", nil)
			return
		}
		err := hs.doScaleMoniter(sm, replica, timeOut, false)
		writeResponse(res, err, "scale success", "scale fail", nil)
	} else {
		writeResponse(res, fmt.Errorf("not support http method:%s", req.Method), "", "scale fail", nil)
	}
}
func (hs *httpServer) clean(res http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		sm, errParse := getScaleMoniterFromBody(req.Body)
		if errParse != nil {
			writeResponse(res, errParse, "", "clean fail", nil)
			return
		}
		err := hs.doScaleMoniter(sm, 0, 0, true)
		log.MyLogI("clean monitor %s, err:%v", sm.Name, err)
		writeResponse(res, err, "clean success", "clean fail", nil)
	} else {
		writeResponse(res, fmt.Errorf("not support http method:%s", req.Method), "", "clean fail", nil)
	}
}
func getScaleMoniterFromBody(read io.ReadCloser) (*common.ScaleMoniter, error) {
	result, _ := ioutil.ReadAll(read)
	read.Close()
	sm := &common.ScaleMoniter{}
	err := json.Unmarshal(result, sm)
	if err != nil {
		return nil, err
	}
	return sm, common.ValidationScaleMoniter(sm)
}
func installHttpServer(serverAddr, tokenfile, certfile, keyfile string, scaleManager *scaleplugin.ScaleManager) error {
	mux := http.NewServeMux()
	auth, err := newAuthenticatorFromBasicAuthFile(tokenfile)
	if err != nil {
		log.MyLogE("Unable to installHttpServer: %v", err)
		return err
	}

	handler := WithAuthentication(mux, auth)
	mux.HandleFunc("/version", Version)

	httpServer := &httpServer{scaleManager: scaleManager}

	mux.HandleFunc("/scale/", httpServer.scale)
	mux.HandleFunc("/clean", httpServer.clean)
	mux.HandleFunc("/isscaling", httpServer.isScale)
	mux.HandleFunc("/replica", httpServer.getReplica)

	server := &http.Server{Addr: serverAddr, Handler: handler}
	return server.ListenAndServeTLS(certfile, keyfile)
}

func writeResponse(w io.Writer, err error, msgOk, msgFail string, object interface{}) {
	var retCode int
	var retErrStr string
	var objStr string
	var msg string
	if err != nil {
		retCode = 201
		retErrStr = err.Error()
		msg = msgFail
	} else {
		retCode = 200
		retErrStr = ""
		msg = msgOk
	}
	if object != nil && err == nil {
		buf, _ := json.MarshalIndent(object, " ", "  ")
		objStr = string(buf)

	}

	tmp := fmt.Sprintf("{\"code\":%d,\"msg\":\"%s\",\"err\":\"%s\", \"data\":\"%s\"}",
		retCode, msg, retErrStr, strings.Replace(objStr, "\"", "\\\"", -1))
	w.Write([]byte(tmp))
}

func Version(res http.ResponseWriter, req *http.Request) {
	res.Write([]byte(version.Get().String()))
}

// newAuthenticatorFromBasicAuthFile returns an authenticator.Request or an error
func newAuthenticatorFromBasicAuthFile(basicAuthFile string) (authenticator.Request, error) {
	basicAuthenticator, err := passwordfile.NewCSV(basicAuthFile)
	if err != nil {
		return nil, err
	}

	return basicauth.New(basicAuthenticator), nil
}

func WithAuthentication(handler http.Handler, auth authenticator.Request) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		_, ok, err := auth.AuthenticateRequest(req)
		if ok == false || err != nil {
			if err != nil {
				log.MyLogE("Unable to authenticate the request due to an error: %v", err)
			}
			unauthorizedBasicAuth(w, req)
			return
		}
		handler.ServeHTTP(w, req)
	})
}

// unauthorizedBasicAuth serves an unauthorized message to clients.
func unauthorizedBasicAuth(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("WWW-Authenticate", `Basic realm="kubernetes-master"`)
	http.Error(w, "Unauthorized", http.StatusUnauthorized)
}
