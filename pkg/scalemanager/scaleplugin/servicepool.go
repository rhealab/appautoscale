package scaleplugin

import (
	"appautoscale/common"
	//"appautoscale/log"
	"fmt"
	"strconv"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/kubernetes/pkg/api"
	v1 "k8s.io/kubernetes/pkg/api/v1"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

const (
	svcIndexAnn string = "io.enndata.appautoscale/svc_index"
)

type SvcPoolInterface interface {
	IsServiceExist(index int) (bool, error)
	GetServiceLabelsByIndex(index int) (map[string]string, error)
	CreateService(index int) error
	GetServicesByIndexs(indexs []int) ([]*v1.Service, error)
	CreateAndGetServiceLabels(index int) (map[string]string, error)
	GetServiceNum() (int, error)
}

type ServicePool struct {
	namespaces string
	svcPool    *common.SvcPool
	client     *clientset.Clientset
}

func NewServicePool(ns string, pool *common.SvcPool, client *clientset.Clientset) *ServicePool {
	return &ServicePool{
		namespaces: ns,
		svcPool:    pool,
		client:     client,
	}
}
func (sp *ServicePool) getSvcIndexAnn() string {
	ann := svcIndexAnn
	if sp.svcPool != nil && sp.svcPool.IndexAnnotationKey != "" {
		ann = sp.svcPool.IndexAnnotationKey
	}
	return ann
}
func (sp *ServicePool) getSvcIndex(svc *v1.Service) int {
	ann := sp.getSvcIndexAnn()
	if svc == nil || svc.Annotations == nil || svc.Annotations[ann] == "" {
		return -1
	}
	indexStr := svc.Annotations[ann]
	if index, err := strconv.Atoi(indexStr); err != nil {
		return -1
	} else {
		return index
	}
}
func (sp *ServicePool) GetServicesByIndexs(indexs []int) ([]*v1.Service, error) {
	svcs, err := sp.getAndFilterSvcs()
	if err != nil {
		return []*v1.Service{}, err
	}

	svcMap := make(map[int]*v1.Service)
	for _, svc := range svcs {
		i := sp.getSvcIndex(svc)
		if i >= 0 {
			svcMap[i] = svc
		}
	}
	ret := make([]*v1.Service, 0, len(indexs))
	for _, index := range indexs {
		if svc, exist := svcMap[index]; exist == true && svc != nil {
			ret = append(ret, svc)
		} else {
			return ret, fmt.Errorf("service index %d is not found", index)
		}
	}
	return ret, nil
}

func (sp *ServicePool) IsServiceExist(index int) (bool, error) {
	if sp.svcPool == nil || (sp.svcPool.Selector == nil && sp.svcPool.SvcTemplate == nil) {
		return false, nil
	}
	svcs, err := sp.getAndFilterSvcs()
	if err != nil {
		return false, err
	}
	for _, svc := range svcs {
		i := sp.getSvcIndex(svc)
		if i == index {
			return true, nil
		}
	}
	return false, nil
}

func (sp *ServicePool) getAndFilterSvcs() ([]*v1.Service, error) {
	svcList, errList := sp.client.CoreV1().Services(sp.namespaces).List(metav1.ListOptions{})
	if errList != nil {
		return []*v1.Service{}, fmt.Errorf("getAndFilterSvcs list svc of %s namespace :%v", sp.namespaces, errList)
	}

	return sp.fileterSvc(svcList.Items)
}

func (sp *ServicePool) fileterSvc(list []v1.Service) ([]*v1.Service, error) {
	if sp.svcPool == nil {
		return []*v1.Service{}, nil
	}
	var selector labels.Selector
	var err error
	if sp.svcPool.Selector != nil {
		selector, err = metav1.LabelSelectorAsSelector(sp.svcPool.Selector)
		if err != nil {
			return []*v1.Service{}, err
		}
	} else if sp.svcPool.SvcTemplate != nil {
		selector, err = metav1.LabelSelectorAsSelector(&metav1.LabelSelector{MatchLabels: sp.svcPool.SvcTemplate.Spec.Selector})
		if err != nil {
			return []*v1.Service{}, err
		}
		//selector = &templateSelector
	} else {
		return []*v1.Service{}, nil
	}

	ret := make([]*v1.Service, 0, len(list))
	for i := range list {
		labels := labels.Set(list[i].Spec.Selector)
		if selector.Matches(labels) && sp.getSvcIndex(&list[i]) > 0 {
			ret = append(ret, &list[i])
		}
	}
	return ret, nil
}

func (sp *ServicePool) GetServiceLabelsByIndex(index int) (map[string]string, error, bool) {
	svcs, err := sp.getAndFilterSvcs()
	if err != nil {
		return map[string]string{}, err, false
	}
	for _, svc := range svcs {
		i := sp.getSvcIndex(svc)
		if index == i {
			return svc.Spec.Selector, nil, false
		}
	}
	return map[string]string{}, fmt.Errorf("svc pool not found %d svc", index), true
}

func (sp *ServicePool) getSvcByIndexFromTemplate(index int) (*v1.Service, error) {
	if sp.svcPool == nil || sp.svcPool.SvcTemplate == nil {
		return nil, fmt.Errorf("svcPool.SvcTemplate is nil")
	}
	copy, err := api.Scheme.DeepCopy(sp.svcPool.SvcTemplate)
	if err != nil {
		return nil, err
	}
	service := copy.(*v1.Service)
	service.Name = fmt.Sprintf("%s-%d", service.Name, index)
	if service.Annotations == nil {
		service.Annotations = make(map[string]string)
	}
	indexAnn := sp.getSvcIndexAnn()
	service.Annotations[indexAnn] = strconv.Itoa(index)

	if service.Spec.Selector == nil {
		service.Spec.Selector = make(map[string]string)
	}
	service.Spec.Selector[indexAnn] = strconv.Itoa(index)

	if service.Labels == nil {
		service.Labels = make(map[string]string)
	}
	service.Labels[indexAnn] = strconv.Itoa(index)
	return service, nil
}

func (sp *ServicePool) CreateService(index int) (bool, error) {
	exist, err := sp.IsServiceExist(index)
	if err != nil {
		return false, err
	}
	if exist == true {
		return true, fmt.Errorf("service index %d is exist", index)
	}
	if sp.svcPool == nil || sp.svcPool.SvcTemplate == nil {
		//log.MyLogD("SvcTemplate is no defined cann't create svc %d", index)
		return false, nil
	}
	svc, errGet := sp.getSvcByIndexFromTemplate(index)
	if errGet != nil || svc == nil {
		return false, fmt.Errorf("getSvcByIndexFromTemplate err:%v", errGet)
	}

	_, errCreate := sp.client.CoreV1().Services(sp.namespaces).Create(svc)
	if errCreate != nil {
		return false, errCreate
	}
	return true, nil
}

func (sp *ServicePool) DeleteSvc(index int) error {
	if sp.svcPool == nil || sp.svcPool.SvcTemplate == nil || sp.svcPool.Selector != nil {
		return nil
	}
	svcs, err := sp.getAndFilterSvcs()
	if err != nil {
		return err
	}
	for _, svc := range svcs {
		i := sp.getSvcIndex(svc)
		if i == index {
			return sp.client.CoreV1().Services(sp.namespaces).Delete(svc.Name, nil)
		}
	}
	return nil
}

func (sp *ServicePool) CreateAndGetServiceLabels(index int) (map[string]string, error) {
	if exist, _ := sp.IsServiceExist(index); exist == true {
		labels, err, _ := sp.GetServiceLabelsByIndex(index)
		return labels, err
	} else {
		if ok, errCreate := sp.CreateService(index); errCreate != nil {
			return map[string]string{}, errCreate
		} else if ok == false {
			return map[string]string{}, nil
		} else {
			labels, err, _ := sp.GetServiceLabelsByIndex(index)
			return labels, err
		}
	}
}
func (sp *ServicePool) GetServiceNum() (int, error) {
	svcs, err := sp.getAndFilterSvcs()
	if err != nil {
		return 0, err
	}
	return len(svcs), nil
}
