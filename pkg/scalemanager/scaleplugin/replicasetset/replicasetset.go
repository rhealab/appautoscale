package replicasetset

import (
	"appautoscale/common"
	"appautoscale/errors"
	"appautoscale/log"
	"appautoscale/pkg/monitor/metricplugin"
	"appautoscale/pkg/scalemanager/scaleplugin"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/kubernetes/pkg/api"
	"k8s.io/kubernetes/pkg/api/v1"
	extensions "k8s.io/kubernetes/pkg/apis/extensions/v1beta1"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

const (
	pluginName         string = "ReplicaSetSet"
	replicaSetIndexAnn string = "io.enndata.appautoscale/replicaset_index"
	replicaSetSortAnn  string = "io.enndata.appautoscale/replicaset_sort"
)

func ProbeVolumePlugins() []scaleplugin.ScalePlugin {
	return []scaleplugin.ScalePlugin{
		&replicaSetSet{},
	}
}

var _ scaleplugin.ScalePlugin = &replicaSetSet{}
var _ scaleplugin.Scale = &replicaSetScale{}

type ReplicaSetDeleteSort struct {
	rss            []*extensions.ReplicaSet
	scoresMap      map[string]float64
	pods           []*v1.Pod
	mm             *metricplugin.MetricManager
	moniterMetrics []common.MoniterMetric
	metricSorts    []common.MetricSort
}

func (rsds *ReplicaSetDeleteSort) getScore(i int) float64 {
	if score, exist := rsds.scoresMap[rsds.rss[i].Name]; exist == true {
		return score
	} else {
		if rsds.pods == nil || len(rsds.pods) == 0 {
			log.MyLogD("getScore of %d return1", i)
			return 0.0
		}
		pods, err := getReplicaSetPods(rsds.rss[i], rsds.pods)
		if err != nil {
			log.MyLogD("getScore of %d return2: err:%v", i, err)
			return 0.0
		}
		moniterMetricMap := make(map[string]common.MoniterMetric)
		for _, mm := range rsds.moniterMetrics {
			moniterMetricMap[mm.Name] = mm
		}
		var metricScoreSum float64
		for _, metric := range rsds.metricSorts {
			if mm, exist := moniterMetricMap[metric.MetricName]; exist == false {
				log.MyLogD("getScore of %d return3: %s not exist", i, metric.MetricName)
				return 0.0
			} else {
				plugin, err := rsds.mm.FindPlugin(&mm.MetricGeter)
				if err != nil {
					log.MyLogD("getScore of %d return4: %s plugin not exist", i, metric.MetricName)
					return 0.0
				}
				getterList := make([]metricplugin.MetricGeter, 0, len(pods))
				for _, pod := range pods {
					getter, errNew := plugin.NewMetricGeter(&mm, pod)
					if errNew != nil {
						log.MyLogD("getScore of %d return4: %s plugin NewMetricGeter err:%v", i, metric.MetricName, errNew)
						return 0.0
					}
					getterList = append(getterList, getter)
				}
				mgl := &metricplugin.MetricGeterList{
					MetricName:     mm.Name,
					Getters:        getterList,
					Duration:       mm.MetricsTime,
					UpScaleValue:   mm.UpScaleValue,
					DownScaleValue: mm.DownScaleValue,
				}
				score, errGetScore := mgl.GetMetricScore()
				if errGetScore == nil {
					metricScoreSum += score * float64(metric.Weight)
				}
			}
		}
		rsds.scoresMap[rsds.rss[i].Name] = metricScoreSum
		return metricScoreSum
	}
}

func (rsds *ReplicaSetDeleteSort) Len() int {
	return len(rsds.rss)
}
func (rsds *ReplicaSetDeleteSort) Swap(i, j int) {
	rsds.rss[i], rsds.rss[j] = rsds.rss[j], rsds.rss[i]
}

func (rsds *ReplicaSetDeleteSort) Less(i, j int) bool {
	if rsds.rss[i].Status.AvailableReplicas != rsds.rss[j].Status.AvailableReplicas {
		return rsds.rss[i].Status.AvailableReplicas < rsds.rss[j].Status.AvailableReplicas
	}
	ri := *rsds.rss[i].Spec.Replicas
	rj := *rsds.rss[j].Spec.Replicas
	if ri == 0 {
		return true
	}
	if rj == 0 {
		return false
	}
	wi := getReplicaSetSortWeight(rsds.rss[i])
	wj := getReplicaSetSortWeight(rsds.rss[j])
	if wi != wj {
		return wi > wj
	}
	scorei := rsds.getScore(i)
	scorej := rsds.getScore(j)
	if common.IsFloatEqual(scorei, scorej) != true {
		return scorei < scorej
	}
	return getReplicaSetIndex(rsds.rss[i]) > getReplicaSetIndex(rsds.rss[j])
}

type replicaSetSet struct {
	client *clientset.Clientset
	mm     *metricplugin.MetricManager
}

func (rss *replicaSetSet) PluginName() string {
	return pluginName
}

func (rss *replicaSetSet) Init(client *clientset.Clientset, mm *metricplugin.MetricManager) error {
	if client == nil {
		return fmt.Errorf("replicaSetSet int client == nil")
	}
	rss.client = client
	rss.mm = mm
	return nil
}

func (rss *replicaSetSet) NewScale(sm *common.ScaleMoniter) (scaleplugin.Scale, error) {
	if sm.ScaleTemplate.ReplicaSetSet == nil {
		return nil, fmt.Errorf("ScaleTemplate.ReplicaSetSet is nil")
	}

	return &replicaSetScale{
		rs:               sm.ScaleTemplate.ReplicaSetSet,
		client:           rss.client,
		mm:               rss.mm,
		canDeleteTrigger: sm.CanDeleteTrigger,
		moniterMetrics:   sm.MoniterMetrics,
		metricSorts:      sm.SvcMetricSort,
		maxReplica:       sm.MaxReplics,
		minReplica:       sm.MinReplics,
		svcPool:          scaleplugin.NewServicePool(sm.ScaleTemplate.ReplicaSetSet.Namespace, sm.SvcPool, rss.client),
	}, nil
}

type replicaSetScale struct {
	rs                     *extensions.ReplicaSet
	canDeleteTrigger       string
	metricSorts            []common.MetricSort
	moniterMetrics         []common.MoniterMetric
	client                 *clientset.Clientset
	mm                     *metricplugin.MetricManager
	maxReplica, minReplica int
	svcPool                *scaleplugin.ServicePool
}

func getReplicaSetIndex(rs *extensions.ReplicaSet) int {
	if rs == nil || rs.Annotations == nil || rs.Annotations[replicaSetIndexAnn] == "" {
		return -1
	}
	indexStr := rs.Annotations[replicaSetIndexAnn]
	if index, err := strconv.Atoi(indexStr); err != nil {
		return -1
	} else {
		return index
	}
}
func getReplicaSetSortWeight(rs *extensions.ReplicaSet) int {
	if rs == nil || rs.Annotations == nil || rs.Annotations[replicaSetSortAnn] == "" {
		return 0
	}
	weightStr := rs.Annotations[replicaSetSortAnn]
	if weight, err := strconv.Atoi(weightStr); err != nil {
		return 0
	} else {
		return weight
	}
}
func (rss *replicaSetScale) getPods() ([]*v1.Pod, error) {
	podList, err := rss.client.CoreV1().Pods(rss.rs.Namespace).List(metav1.ListOptions{})
	if err != nil {
		return []*v1.Pod{}, fmt.Errorf("get pod from ns %s err:%v", rss.rs.Namespace, err)
	}
	ret := make([]*v1.Pod, 0, len(podList.Items))
	for i := range podList.Items {
		ret = append(ret, &podList.Items[i])
	}
	return ret, nil
}

func (rss *replicaSetScale) isPodCanBeDeleted(pod *v1.Pod) bool {
	if rss.canDeleteTrigger == "" { // no define canDeleteTrigger all pod can be deleted
		log.MyLogD("canDeleteTrigger is empty all pod can be deleted")
		return true
	}
	if common.IsReadyPod(pod) == false {
		return true
	}
	mgls := make([]*metricplugin.MetricGeterList, 0, len(rss.moniterMetrics))

	for _, metric := range rss.moniterMetrics {
		plugin, err := rss.mm.FindPlugin(&metric.MetricGeter)
		if err != nil {
			log.MyLogE("metric %s cann't find plugin", metric.Name)
			return false
		}
		getter, errNew := plugin.NewMetricGeter(&metric, pod)
		if errNew != nil {
			log.MyLogE("plugin %s NewMetricGeter for pod %s:%s err:%v", plugin.PluginName(), pod.Namespace, pod.Name, errNew)
			return false
		}
		mgl := &metricplugin.MetricGeterList{
			MetricName:     metric.Name,
			Getters:        []metricplugin.MetricGeter{getter},
			Duration:       metric.MetricsTime,
			UpScaleValue:   metric.UpScaleValue,
			DownScaleValue: metric.DownScaleValue,
		}
		mgls = append(mgls, mgl)
	}
	mt := &metricplugin.MetricTrigger{
		Mgls:        mgls,
		Downtrigger: rss.canDeleteTrigger,
	}
	//log.MyLogD("pod:%s:%s trigger:%s, len(mgls)=%d", pod.Namespace, pod.Name, mt.Downtrigger, len(mt.Mgls))
	if ok, errDown := mt.IsDown(); ok == true && errDown == nil {
		return true
	} else if ok == false && errDown == nil {
		log.MyLogD("pod %s:%s cann't be delete because trigger:%s", pod.Namespace, pod.Name, rss.canDeleteTrigger)
	} else {
		log.MyLogE("pod %s:%s cann't be delete bacause check err:%v", pod.Namespace, pod.Name, errDown)
	}
	return false
}

func getReplicaSetPods(rs *extensions.ReplicaSet, podList []*v1.Pod) ([]*v1.Pod, error) {
	selector, err := metav1.LabelSelectorAsSelector(rs.Spec.Selector)
	if err != nil {
		return []*v1.Pod{}, err
	}
	ret := make([]*v1.Pod, 0, len(podList))
	for _, pod := range podList {
		labels := labels.Set(pod.Labels)
		if selector.Matches(labels) {
			ret = append(ret, pod)
		}
	}
	return ret, nil
}

func (rss *replicaSetScale) isReplicaSetCanDelete(rs *extensions.ReplicaSet, podList []*v1.Pod) bool {
	pods, err := getReplicaSetPods(rs, podList)
	if err != nil {
		log.MyLogE("get pods of repliaset %s:%s err:%v", rs.Namespace, rs.Name, err)
		return false
	}
	log.MyLogD("isReplicaSetCanDelete podlist:%d, pods:%d of repliaset:%s:%s", len(podList), len(pods), rs.Namespace, rs.Name)
	for _, pod := range pods {
		if rss.isPodCanBeDeleted(pod) == false {
			return false
		}
	}
	return true
}
func (rss *replicaSetScale) fileterCanDeleteReplicaSet(list []*extensions.ReplicaSet) []*extensions.ReplicaSet {
	pods, err := rss.getPods()
	if err != nil {
		log.MyLogE("fileterCanDeleteReplicaSet getPods err:%v", err)
		return []*extensions.ReplicaSet{}
	}
	ret := make([]*extensions.ReplicaSet, 0, len(list))
	for _, r := range list {
		if rss.isReplicaSetCanDelete(r, pods) == true {
			ret = append(ret, r)
		}
	}
	return ret
}
func (rss *replicaSetScale) Clean() error {
	return rss.scale(0, nil)
}
func (rss *replicaSetScale) ScaleTo(replica int) error {
	if replica > rss.maxReplica || replica < rss.minReplica {
		return fmt.Errorf("cannot scale to %d no in [%d, %d]", replica, rss.minReplica, rss.maxReplica)
	}
	return rss.scale(replica, nil)
}

func (rss *replicaSetScale) WaitScaleTo(replica int, timeout time.Duration) error {
	if replica > rss.maxReplica || replica < rss.minReplica {
		return fmt.Errorf("cannot scale to %d no in [%d, %d]", replica, rss.minReplica, rss.maxReplica)
	}
	return rss.scale(replica, &timeout)
}

func (rss *replicaSetScale) getAndFilterReplicaSets() ([]*extensions.ReplicaSet, error) {
	rsList, errList := rss.client.Extensions().ReplicaSets(rss.rs.Namespace).List(metav1.ListOptions{})
	if errList != nil {
		return []*extensions.ReplicaSet{}, fmt.Errorf("fileterReplicaSets list replicas of %s namespace :%v", rss.rs.Namespace, errList)
	}

	return rss.fileterReplicaSets(rsList.Items)
}

func (rss *replicaSetScale) fileterReplicaSets(list []extensions.ReplicaSet) ([]*extensions.ReplicaSet, error) {
	selector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{MatchLabels: rss.rs.Spec.Template.Labels})
	if err != nil {
		return []*extensions.ReplicaSet{}, err
	}
	ret := make([]*extensions.ReplicaSet, 0, len(list))
	for i := range list {
		labels := labels.Set(list[i].Labels)
		if selector.Matches(labels) && getReplicaSetIndex(&list[i]) > 0 {
			ret = append(ret, &list[i])
		}
	}
	return ret, nil
}
func (rss *replicaSetScale) IsScaling() (bool, error) {
	filterList, errFilter := rss.getAndFilterReplicaSets()
	if errFilter != nil {
		return false, fmt.Errorf("getAndFilterReplicaSets err:%v", errFilter)
	}
	for _, rs := range filterList {
		replica := rs.Status.Replicas
		if rs.Spec.Replicas != nil {
			replica = *rs.Spec.Replicas
		}
		if rs.Status.AvailableReplicas != replica {
			return true, nil
		}
	}
	return false, nil
}

func (rss *replicaSetScale) GetCurReplica() (cur, wanted int, err error) {
	filterList, errFilter := rss.getAndFilterReplicaSets()
	if errFilter != nil {
		return -1, -1, fmt.Errorf("getAndFilterReplicaSets err:%v", errFilter)
	}
	for _, rs := range filterList {
		replica := rs.Status.Replicas
		if rs.Spec.Replicas != nil {
			replica = *rs.Spec.Replicas
		}
		cur += int(rs.Status.AvailableReplicas)
		wanted += int(replica)
	}
	return
}

func mergeLabels(to map[string]string, addLabel map[string]string) error {
	for k, v := range addLabel {
		if existValue, exist := to[k]; exist && existValue != v {
			return fmt.Errorf("%s is exist valuse is %s, cann't set to %s", k, existValue, v)
		} else if exist == false {
			to[k] = v
		}
	}
	return nil
}

func (rss *replicaSetScale) genReplicaSetByIndex(index int, addLabels map[string]string) (*extensions.ReplicaSet, error) {
	newObj, err := api.Scheme.DeepCopy(rss.rs)
	if err != nil {
		return nil, err
	}
	rs := newObj.(*extensions.ReplicaSet)
	rs.Name = fmt.Sprintf("%s-%d", rs.Name, index)
	if rs.Annotations == nil {
		rs.Annotations = make(map[string]string)
	}
	rs.Annotations[replicaSetIndexAnn] = strconv.Itoa(index)
	if rs.Spec.Template.Labels == nil {
		rs.Spec.Template.Labels = make(map[string]string)
	}
	rs.Spec.Template.Labels[common.ReplicaSetIndexLabelName] = strconv.Itoa(index)
	errMerge1 := mergeLabels(rs.Spec.Template.Labels, addLabels)
	if errMerge1 != nil {
		return nil, fmt.Errorf("merge replciaset %s Spec.Template.Labels err:%v", rs.Name, errMerge1)
	}
	if rs.Spec.Selector == nil {
		rs.Spec.Selector = &metav1.LabelSelector{}
	}
	if rs.Spec.Selector.MatchLabels == nil {
		rs.Spec.Selector.MatchLabels = make(map[string]string)
	}
	rs.Spec.Selector.MatchLabels[common.ReplicaSetIndexLabelName] = strconv.Itoa(index)
	errMerge2 := mergeLabels(rs.Spec.Selector.MatchLabels, addLabels)
	if errMerge2 != nil {
		return nil, fmt.Errorf("merge replciaset %s Spec.Selector.MatchLabels err:%v", rs.Name, errMerge2)
	}
	return rs, nil
}

func (rss *replicaSetScale) deleteReplicaSet(rs *extensions.ReplicaSet, timeOut time.Duration) error {
	rs, errGet := rss.client.Extensions().ReplicaSets(rs.Namespace).Get(rs.Name, metav1.GetOptions{})
	if errGet != nil {
		return errGet
	}
	if *rs.Spec.Replicas != 0 {
		var size int32 = 0
		rs.Spec.Replicas = &size
		_, err := rss.client.Extensions().ReplicaSets(rs.Namespace).Update(rs)
		if err != nil {
			return err
		}
	}

	waitFun := func() (bool, error) {
		rs, err := rss.client.Extensions().ReplicaSets(rs.Namespace).Get(rs.Name, metav1.GetOptions{})
		if err != nil {
			return false, err
		}
		return rs.Status.AvailableReplicas == 0, nil
	}

	errWait := wait.Poll(time.Second, timeOut, waitFun)
	if errWait == wait.ErrWaitTimeout {
		return fmt.Errorf("delete replicaset %s wait UpdatedReplicas == 0 timeout", rs.Name)
	}
	if errWait != nil {
		return errWait
	}
	return rss.client.Extensions().ReplicaSets(rs.Namespace).Delete(rs.Name, nil)
}

func (rss *replicaSetScale) scaleSimple(newSize int) (needTry bool, err error) {
	filterList, errFilter := rss.getAndFilterReplicaSets()
	if errFilter != nil {
		return true, fmt.Errorf("getAndFilterReplicaSets err:%v", errFilter)
	}
	indexMap := make(map[int]bool)
	for _, d := range filterList {
		index := getReplicaSetIndex(d)
		indexMap[index] = true
	}
	indexGet := func() int {
		for i := 1; i <= rss.maxReplica; i++ {
			if exist := indexMap[i]; exist == false {
				return i
			}
		}
		return -1
	}
	if len(filterList) == newSize {
		return false, nil
	} else if len(filterList) < newSize {
		for i := len(filterList); i < newSize; i++ {
			index := indexGet()
			labels, errCG := rss.svcPool.CreateAndGetServiceLabels(index)
			if errCG != nil {
				return true, fmt.Errorf("create svc %d err:%v", index, errCG)
			}
			rs, errGet := rss.genReplicaSetByIndex(index, labels)
			if errGet != nil {
				return true, fmt.Errorf("genReplicaSetByIndex %d err:%v", index, errGet)
			}
			_, errCreate := rss.client.Extensions().ReplicaSets(rs.Namespace).Create(rs)
			if errCreate != nil {
				return true, fmt.Errorf("create replicaset %d err:%v", index, errCreate)
			}
			indexMap[index] = true
		}
	} else {
		deleteSize := len(filterList) - newSize
		canDeleteList := rss.fileterCanDeleteReplicaSet(filterList)
		if deleteSize > len(canDeleteList) {
			deleteSize = len(canDeleteList)
		}
		pods, _ := rss.getPods()
		rsds := &ReplicaSetDeleteSort{
			rss:            canDeleteList,
			scoresMap:      make(map[string]float64),
			pods:           pods,
			mm:             rss.mm,
			moniterMetrics: rss.moniterMetrics,
			metricSorts:    rss.metricSorts,
		}
		sort.Sort(rsds)
		var wg sync.WaitGroup
		errstrs := make([]string, deleteSize)
		for i := 0; i < deleteSize; i++ {
			wg.Add(1)
			go func(rs *extensions.ReplicaSet, index int) {
				defer wg.Done()
				rsIndex := getReplicaSetIndex(rs)
				errDeleteSvc := rss.svcPool.DeleteSvc(rsIndex)
				if errDeleteSvc != nil {
					errstrs[index] = fmt.Sprintf("delete svc %d:%s", rsIndex, errDeleteSvc.Error())
					return
				}
				err := rss.deleteReplicaSet(rs, 10*time.Second)
				if err != nil {
					errstrs[index] = fmt.Sprintf("%d:%s", index, err.Error())
				} else {
					errstrs[index] = ""
				}
			}(rsds.rss[i], i)
		}
		wg.Wait()
		strs := common.FilterEmptyString(errstrs)
		if len(strs) != 0 {
			return true, fmt.Errorf("scaleSimple err:[%s]", strings.Join(strs, ","))
		}
		if deleteSize != len(filterList)-newSize {
			return false,
				errors.NewScaleDeleteMetricNotMatch(
					fmt.Sprintf("%d replicaset cann't be scale down because not match trigger %s",
						len(filterList)-newSize-deleteSize, rss.canDeleteTrigger))

		}
	}
	return false, nil
}

func (rss *replicaSetScale) scale(newSize int, waitTimeOut *time.Duration) error {
	tryScale := func() (bool, error) {
		needTry, err := rss.scaleSimple(newSize)
		if err != nil {
			log.MyLogE("scaleSimple err:%v", err)
			if needTry {
				return false, nil
			} else {
				return true, err
			}
		}
		return true, nil
	}

	if err := wait.Poll(100*time.Millisecond, 1*time.Minute, tryScale); err != nil {
		return err
	}

	if waitTimeOut != nil {
		filterList, errFilter := rss.getAndFilterReplicaSets()
		if errFilter != nil {
			return fmt.Errorf("getAndFilterReplicaSets err:%v", errFilter)
		}
		waitMap := make(map[string]int64)
		for _, d := range filterList {
			waitMap[d.Name] = d.Generation
		}
		waitFun := func() (bool, error) {
			for name, generation := range waitMap {
				rs, err := rss.client.Extensions().ReplicaSets(rss.rs.Namespace).Get(name, metav1.GetOptions{})
				if err != nil {
					return false, err
				}
				replica := rs.Status.Replicas
				if rs.Spec.Replicas != nil {
					replica = *rs.Spec.Replicas
				}

				if rs.Status.ObservedGeneration >= generation && rs.Status.AvailableReplicas == replica {
					delete(waitMap, name)
				}
			}
			return true, nil
		}
		errWait := wait.Poll(time.Second, *waitTimeOut, waitFun)
		if errWait == wait.ErrWaitTimeout {
			return fmt.Errorf("timed out waiting replicaset %s synced", rss.rs.Name)
		}
		return errWait
	}
	return nil
}
